## DEFINITIONS
- The Python Standard Library module `functools` contains several higher-order functions. 
- *Higher-order functions* are functions which act on or return other functions 
    - A function decorator is an example of a higher-order function 



## HISTORY
- The `functools` module was added in Python 2.5 (2006)
    - `wraps`, `update_wrapper` and `partial`
- Additional functions continue to be added 
    - Python 3.0: `reduce` 
    - Python 3.2: `total_ordering` and `cmp_to_key`, `lru_cache`
    - Python 3.4: `partialmethod`, `singledispatch`
    - Python 3.8: `cached_property`, `singledispatchmethod`
    - Python 3.9: `cache`



## SIMPLIFYING FUNCTION SIGNATURES
- `partial`
- `partialmethod`



### partial
- `partial(func, *args, **keywords)`
- Taks as input a function and the arguments to "lock in"
- Returns a `partial object` which behaviors like the original function called with those arguments already defined. 
```python
from functools import partial
pow_2 = partial(pow, exp=2)
print(f"{pow_2(5)=}")       # pow_2(5)=25
```



#### REDUCE ARGUMENTS
- Use `partial` to transform a multi-arguments function to a single argument function in places
where that is required (e.g. `map`)
- Possible with `partial`, but is more verbose
```python
from functools import partial
list(map(partial(pow, exp=3), range(10)))   # [0, 1, 8, 27, 64, 125, 216, 343, 512, 729]

def pow_3(x): return pow(x, 3)
list(map(pow_3, range(10)))     # [0, 1, 8, 27, 64, 125, 216, 343, 512, 729]

list(map(lambda x: pow(x 3), range(10)))    # [0, 1, 8, 27, 64, 125, 216, 343, 512, 729]
```



#### SIMPLIFY CODE
- Define functions that are easier to type, read, and friendly to code completion
```python
import sys
from functools import partial

print_stderr = partial(print, file=sys.stderr)
print_stderr("This output goes to stderr")
```



### partialmethod
- `partialmethod(func, *args, **keywords)`
- Easiest to think of it as `partial` for methods
- From the Python docs 
> Returns a new `partialmethod` descriptor which behaves like `partial` except that it is designed to be used as a method definition rather than being directly callable. 
- The function argument must be a descriptor or a callable
```python
# Example code from docs

from functools import partialmethod

class Cell:
    def __init__(self):
        self._alive = False

    @property
    def alive(self):
        return self._alive

    def set_state(self, state):
        self._alive = bool(state)

    set_alive = partialmethod(set_state, True)
    set_dead  = partialmethod(set_state, False)

c = Cell()
c.alive     # False
c.set_alive()
c.alive     # True
```



## FUNCTION WRAPPERS
- `wraps`
- `update_wrapper`



### wraps
- `wraps(wrapped)`
- A function decorator used when defining a wrapper function
- Updates the wrapper function attributes to be the same as the `wrapped` function
- Convenience decoreator factory defined using `update_wrapper()`
```python
# WITHOUT wraps
def my_decorator(f):
    def wrapper(*args, **kwargs):
        """wrapper doc string"""
        print("wrapper called")
        return f(*args, **kwargs)
    return wrapper

@my_decorator
def func():
    """func doc string"""
    print('func called')

func()
# wrapper called
# func called
func.__name__       # 'wrapper'
func.__doc__        # 'wrapper doc string'


# USING wraps
from functools import wraps

def my_decorator_wraps(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        """wrapper doc string"""
        print("wrapper called")
        return f(*args, **kwargs)
    return wrapper

@my_decorator_wraps
def func():
    """func doc string"""
    print("func called")

func(), func.__name__, func.__doc__
```



### update_wrapper
- `update_wrapper(wrapper, wrapped)`
- Rarely used directly
- Updates the `wrapper` function attributes to be the same as the `wrapped` function
- Useful in situation where `@wraps` cannot be used.
    - Wrapping a function after it is defined
    - wrapping a function you do not own

```python
import string

def my_capwords(s, sep=None):
    """my_capwords docstring"""
    print('my_capwords')
    return string.capwords(s, sep=sep)

my_capwords("spam spam spam")
my_capwords.__name__    # "my_capwords"

from functools import update_wrapper

capwords = update_wrapper(my_capwords, string.capwords)
string.capwords.__name__, capwords.__name__, capwords.__doc__
```



## CACHING
- lru_cache
- cache
- cached_property



### lru_cache
- lru_cache(maxsize=128, typed=False)
- Wraps a function with a memoizing callable
- Saves time when an expensive function is sometimes called with the same arguments
- Caches results of most recent maxsize calls
    - LRU stands for Least Recently Used
- If _typed_ is set to *True*, function arguments of different types will be cached seprately



### lru_cache ATTRIBUTES
- Function is also instrumented with 3 functions.
    - `cache_info` - returns a named tuple showing hits, misses, maxsize and currsize
    - `cache_clear()` - clears/invalidates the cache
    - `cache_parameters()` - new `dict` showing the values for maxsize and typed

```python
from functools import lru_cache

@lru_cache
def fib(n):
    if n < 2:
        return n
    return fib(n-1) + fib(n-2)

[fib(n) for n in range(16)]

fib.cache_info(), fib.cache_parameters()
```



### lru_cache CAVEATS
- Function's positional and keyword arguments must be hashable
    - Underlying storage is a dictionary
- Should only be used with pure functions
    - Same inputs always produce the same output
    - No side-effects



### cache
- Simple lightweight unbounded function cache
- Same as `lru_cache(maxsize=None)`
- Because there is no eviction, it is smaller and faster that `lru_cache()` with a size limit.



### cached_property
- Similar to `property()` with the addition of caching
- Value is computed once and then cached as a normal attribute for the life of the instance
- Unlike `property`, `cached_property()` allows writes without a setter being defined
- `cache_property` only runs on lookup and only if the attribute does not already exist
- Once attribute exists, sebsequent reads and writes work like a normal attribute



```python
import statistics
from functools import cached_property

class DataSet:
  def __init__(self, sequence_of_numbers):
    self._data = tuple(sequence_of_numbers)

  @cached_property
  def stdev(self):
    return statistics.stdev(self._data)

from random import random

seq = (random() for _ in range(10_000_000))
d = DataSet(seq)

start_time = time.time()
d.stdev
end_time = time.time()
print(f"{end_time - start_time}")

start_time = time.time()
d.stdev
end_time = time.time()
print(f"{end_time - start_time}")

```



## ORDERED TYPES
- `total_ordering`



### total_ordering
- Class decorator that makes it easy to create well behaved totally ordered types
- If class defines at least one rich comparison operator, it supplies the rest
    - Class must define one of `__lt__()`, `__le__()`, `__gt__()`, or `__ge__()`
    - Additionally, class should supply an `__eq__()` method
- Caveat: does come at the cost of slower execution and more complex stack traces for the derived comparison methods



### EXAMPLE: total_ordering
```python
from functools import total_ordering

@total_ordering
class Car():
    def __init__(self, year, make, model):
        self.year, self.make, self.model = year, make, model
    def __eq__(self, other):
        if not isinstance(other, Car): return NotImplemented
        return ((self.year, self.make, self.model) == (other.year, other.make, other.model))
    def __lt__(self, other):
        if not isinstance(other, Car): return NotImplemented
        return ((self.year, self.make, self.model) < (other.year, other.make, other.model))


from car import Car

car_1 = Car(2020, 'BMW', '530i')
car_2 = Car(2020, 'BMW', '330i')
(car_1 < car_2), (car_1 > car_2)    # (False, True)
```


## REDUCE
- `reduce`



### reduce
- `reduce(function, iterable[, initializer])`
- Applies function of 2 arguments cumulatively to the items of iterable to reduce it to a single value
    - Built-in function `sum()` is an example of a reducer
- Example:
```python
from functools import reduce
import operator

def product(iterable):
    return reduce(operator.mul, iterable, 1)

produce([2, 5, 8])      # 80
produce(range(2, 5))    # 24
```


## FUNCTION OVERLOADING
- `singledispatch`
- `singledispatchmethod`



### singledispatch
- Function decorator which transforms a function into a singledispatch generic function
    - Means implementation is chosen based on the type of a single argument
- Generic function is decorated with `@singledispatch`
- Overloaded implementions are decorated with the `register()` attribute of the generic function
    - If implementation is annotated with types, the decorator will automatically infer the type of the argument
    - Otherwise, the type is an argument to the decorator



### EXAMPLE: singledispatch
```python
from functools import singledispatch

@singledispatch
def fun(arg): print(f"Let me just say, {arg}")

@fun.register
def _(arg: int): print(f"Got a number: {arg}")

@fun.register
def _(arg: list):
    print(f"Got a list: ")
    for i, elem in enumerate(arg):
        print(i, elem)

@fun.register(complex)
def _(arg):
    print(f"Got a complex. {arg.real} {arg.imag}")


fun(9)
# 
fun([1,2,3])
#
fun(3.4)
#
```



### singledispatchmethod
- Single dispatch for methods
- Function decorator which transforms a method into a singledispatch generic function
    - Dispatch happens on the type of the first non-self or non-cls argument



## CONCLUSION
- The `functools` module contains many higher-order functions which are both useful and powerful
- Using these functions can result in more readable and maintainable code


## REFERENCES

- [Python Language Reference](https://docs.python.org/3/library/functools.html)
- [These slides on GitHub](https://sjirwin.github.io/hitchhikers-guide-to-functools/)
- [Functools - The Power of Higher-Order Functions in Python](https://martinheinz.dev/blog/52)
- [Introduction To Python's Functools Module](https://florian-dahlitz.de/articles/introduction-topythons-functools-module)



